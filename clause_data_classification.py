import boto3
import time
import os 
import json
import pandas as pd
from nltk.tokenize import sent_tokenize
from io import StringIO 
from io import BytesIO
import gzip
from simpletransformers.classification import ClassificationModel
import spacy
from spacy.util import minibatch, compounding
import spacy
from spacy.scorer import Scorer


def startJob(s3BucketName, objectName):
    response = None
    client = boto3.client('textract')
    response = client.start_document_text_detection(
    DocumentLocation={
        'S3Object': {
            'Bucket': s3BucketName,
            'Name': objectName
        }
    })

    return response["JobId"]

def isJobComplete(jobId):
    time.sleep(5)
    client = boto3.client('textract')
    response = client.get_document_text_detection(JobId=jobId)
    status = response["JobStatus"]
    print("Job status: {}".format(status))

    while(status == "IN_PROGRESS"):
        time.sleep(5)
        response = client.get_document_text_detection(JobId=jobId)
        status = response["JobStatus"]
        print("Job status: {}".format(status))

    return status

def getJobResults(jobId):

    pages = []

    time.sleep(5)

    client = boto3.client('textract')
    response = client.get_document_text_detection(JobId=jobId)

    pages.append(response)
    print("Resultset page recieved: {}".format(len(pages)))
    nextToken = None
    if('NextToken' in response):
        nextToken = response['NextToken']

    while(nextToken):
        time.sleep(5)

        response = client.get_document_text_detection(JobId=jobId, NextToken=nextToken)

        pages.append(response)
        print("Resultset page recieved: {}".format(len(pages)))
        nextToken = None
        if('NextToken' in response):
            nextToken = response['NextToken']

    return pages

# OCR using amazon-textract and process ocr file and upload it on s3 bucket
def ocrTextract(bucket, file_name, classifier):
    print('processing %s' % file_name)
    jobId = startJob(bucket, file_name)
    print("Started job with id: {}".format(jobId))
    if(isJobComplete(jobId)):
        response = getJobResults(jobId)

    outfile = file_name.replace('.pdf', '.txt')
    # print(outfile)
    f = open(outfile, "w") 
    print('writing to %s' % outfile)

    # print detected text
    for resultPage in response:
        for item in resultPage["Blocks"]:
            if item["BlockType"] == "LINE":
                f.write(item["Text"])
                f.write("\n")
    f.close()
    s3.upload_file(outfile, bucket, outfile)

    if classifier == 'amazon':
        textToCsv(bucket, outfile)

# takes the text file from s3 bucket and convert it to csv 
def textToCsv(bucket, file_name):
    df = pd.DataFrame() 
    fp = open(file_name)
    about_text = fp.read()
    # # split into sentences
    sentences = sent_tokenize(about_text)
    for sent in range(0,len(sentences)):
        new_row = {'text': sentences[sent]}
        df = df.append(new_row, ignore_index=True)
    df = df.replace({r'\s+$': '', r'^\s+': ''}, regex=True).replace(r'\n',  ' ', regex=True)
    outfile = file_name.replace('.txt', '.csv')
    csv_buffer = StringIO()
    df.to_csv(csv_buffer,header=False, index=False)
    s3_resource = boto3.resource('s3')
    s3_resource.Object(bucket, outfile).put(Body=csv_buffer.getvalue())

    #return makePredictions(bucket) 

# takes csv file from s3 and make the predictions
def makePredictions(bucket):
    
    s3 = boto3.client('s3') 
    response_doc = s3.list_objects(
        Bucket = bucket,
        Delimiter = '.csv'
    )

    fileJobDict = {}
    predicted_labels = {}

    for file in range(0,len(response_doc['CommonPrefixes'])):
        file_name = response_doc['CommonPrefixes'][file]['Prefix']
        input_s3_url = 's3://{}/{}'.format(bucket, file_name)
        input_doc_format = 'ONE_DOC_PER_LINE'
        output_s3_url = 's3://{}/'.format(bucket)
        data_access_role_arn = 'arn:aws:iam::798270774932:role/WhitemarshComprehendRole'
        classifier_arn = 'arn:aws:comprehend:us-east-1:798270774932:document-classifier/ClauseTextClassifier'
        job_name= file_name.replace('.csv', '-job')

        # Set up job configuration
        input_data_config = {'S3Uri': input_s3_url, 'InputFormat': input_doc_format}
        output_data_config = {'S3Uri': output_s3_url}

        # Start classification job
        client = boto3.client('comprehend')
        start_response = client.start_document_classification_job(
            InputDataConfig=input_data_config,
            OutputDataConfig=output_data_config,
            JobName=job_name,
            DataAccessRoleArn=data_access_role_arn,
            DocumentClassifierArn=classifier_arn)

        job_id = start_response['JobId']
        print(f'Started Document Classification Job ID: {job_id}')
        fileJobDict[file_name] = job_id
        

    for job in fileJobDict.keys():
        time.sleep(5)
        describe_response = client.describe_document_classification_job(JobId=fileJobDict[job])
        print(describe_response)
        s3_url=describe_response['DocumentClassificationJobProperties']['OutputDataConfig']['S3Uri']
        key = s3_url.split(bucket + '/')[1]
        job_status = describe_response['DocumentClassificationJobProperties']['JobStatus']
        print("Job status: {}".format(job_status))

        while(job_status == "IN_PROGRESS"):
            time.sleep(5)
            describe_response = client.describe_document_classification_job(JobId=fileJobDict[job])
            job_status = describe_response['DocumentClassificationJobProperties']['JobStatus']
            print("Job status: {}".format(job_status))

        print('complete', job_status)

        predicted_labels[job] = Unzip(bucket, key)
    
    return predicted_labels

# unzips the output file and get the json
def Unzip(bucket, key):

    s3 = boto3.resource('s3')
    predictedlabels=[]
    OutputBucket=bucket
    key=key
    obj = s3.Object(OutputBucket, key)
    with gzip.GzipFile(fileobj=obj.get()["Body"]) as gzipfile:
        content = gzipfile.read()
    content=content.decode(encoding="utf-8")
    first=content.find('{') 
    last=content.rindex('}')
    content=content[first:last+1]
    content=content.split('\n')
    for string in range(0,len(content)):
        content_json = json.loads(content[string])
        predictedlabels.append(content_json['Classes'][0]['Name'])
    return predictedlabels

# choosing the classifier
def labelClassifier(bucket, file_txt, model_bert, model_com_lang, classifier, predicted_labels, file_csv):

    if classifier == 'bert':
        clausePredictionsBert(bucket, file_txt, model_bert, model_com_lang)
    else:
        clausePredictionsAmazon(bucket, predicted_labels, file_csv, model_com_lang)

# takes the predictions and put all of it together
def clausePredictionsAmazon(bucket, predictedlabels, file_csv, model_com_lang):
    obj = s3.get_object(Bucket= bucket, Key= file_csv) 
    df = pd.read_csv(obj['Body'],header=None) 
    df.columns=['Documents']
    df['PredictedLabels']=predictedlabels

    amazonPredictedClauses={}
    comLangClauses={}

    for index, row in df.iterrows():

        predictions_com_lang, raw_outputs_com_lang = model_com_lang.predict([row.Documents])

        if row.PredictedLabels in amazonPredictedClauses:
            amazonPredictedClauses[row.PredictedLabels] += row.Documents
        else:
            amazonPredictedClauses[row.PredictedLabels] = row.Documents
        
        str_predictions_com_lang=str(predictions_com_lang[0])

        if str_predictions_com_lang in comLangClauses:
            comLangClauses[str_predictions_com_lang] += row.Documents

        else:
            comLangClauses[str_predictions_com_lang] = row.Documents
    
    print(amazonPredictedClauses)
    print(comLangClauses)

# clasue and common language predictions using BERT
def clausePredictionsBert(bucket, file_name, model_bert, model_com_lang):
    bertPredictedClauses={}
    comLangClauses={}
    fp = open(file_name)
    about_text = fp.read()
    # # split into sentences
    sentences = sent_tokenize(about_text)

    # making the predictions
    for sent in range(0,len(sentences)):
        predictions_clauses, raw_outputs_clauses = model_bert.predict([sentences[sent]])
        predictions_com_lang, raw_outputs_com_lang = model_com_lang.predict([sentences[sent]])

        str_predictions_bert=str(predictions_clauses[0])

        if str_predictions_bert in bertPredictedClauses:
            bertPredictedClauses[str_predictions_bert] += sentences[sent]

        else:
            bertPredictedClauses[str_predictions_bert] = sentences[sent]

        str_predictions_com_lang=str(predictions_com_lang[0])

        if str_predictions_com_lang in comLangClauses:
            comLangClauses[str_predictions_com_lang] += sentences[sent]

        else:
            comLangClauses[str_predictions_com_lang] = sentences[sent]

    print(bertPredictedClauses)
    print(comLangClauses)

# data points prediction
def dataPrediction(bucket, file_name, nlp2_data):
    data_points={}
    fp = open(file_name)
    about_text = fp.read()

    # split into sentences
    sentences = sent_tokenize(about_text)

    # making the predictions
    for sent in range(0,len(sentences)):
        doc2 = nlp2_data(sentences[sent])
        for ent in doc2.ents:
            label=ent.label_.split('-')
            if label[1] in data_points:
                data_points[label[1]] += ent.text
                data_points[label[1]] +=' '
            else:
                data_points[label[1]] = ent.text
                data_points[label[1]] +=' '

    print(data_points)

# making the s3 bucket connection
s3 = boto3.client('s3') 
bucket = 'whitemarsh-textract-test'
classifier = 'bert'
# classifier = 'amazon'

# getting the pdf files from the s3 bucket
response_pdf = s3.list_objects(
Bucket = bucket,
Delimiter = '.pdf'
)

predicted_labels = []

# taking the pdf files and converting them to OCR
for pdf in range(0,len(response_pdf['CommonPrefixes'])):
    file_pdf = response_pdf['CommonPrefixes'][pdf]['Prefix']
    ocrTextract(bucket, file_pdf, classifier)

if classifier == 'amazon':
    predicted_labels = makePredictions(bucket)

# getting the text files from the s3 bucket
response_txt = s3.list_objects(
Bucket = bucket, 
Delimiter = '.txt'
)

# loading all the models for prediction
model_folder_bert=os.path.join(os.getcwd(), 'output_clauses_and_ammendments/checkpoint-76360-epoch-40/')        # give the clause classification model path
model_bert = ClassificationModel('bert', model_folder_bert, num_labels=10)
model_folder_com_lang=os.path.join(os.getcwd(), 'output_comm_lang/checkpoint-70350-epoch-50/')                  # give the common language model path
model_com_lang=ClassificationModel('bert', model_folder_com_lang, num_labels=10)
model_dir_data=os.path.join(os.getcwd(), 'output_ner')                                                          # give data model path
model_data = spacy.load(model_dir_data)

# calling the prediction functions
for txt in range(0,len(response_txt['CommonPrefixes'])):
    file_txt = response_txt['CommonPrefixes'][txt]['Prefix']
    dataPrediction(bucket, file_txt, model_data)
    file_csv= file_txt.replace('.txt', '.csv')
    if classifier == 'amazon':
        labelClassifier(bucket, file_txt, model_bert, model_com_lang, classifier, predicted_labels[file_csv], file_csv)
    else:
        labelClassifier(bucket, file_txt, model_bert, model_com_lang, classifier, predicted_labels, file_csv)
