
from __future__ import unicode_literals, print_function
# import pandas as pd
import os
import pickle
import plac
import random
from pathlib import Path
import spacy
from spacy.util import minibatch, compounding
import spacy
from spacy.scorer import Scorer

# New entity labels
# Specify the new entity labels which you want to add here
LABEL = ['B-LeaseEnteredDate', 'I-LeaseEnteredDate', 'B-LandLord', 'I-LandLord', 'B-Tenant', 'I-Tenant', 'B-SuiteNumber', 'I-SuiteNumber', 'B-RentableSquare', 'I-RentableSquare', 'B-LeaseDuration', 'I-LeaseDuration', 
        'B-CommencementDate', 'I-CommencementDate', 'B-ExpirationDate', 'I-ExpirationDate', 'B-FixedRentAmount', 'I-FixedRentAmount', 
        'B-SecurityDepositAmount', 'I-SecurityDepositAmount', 'B-ElectricExpenseAmount', 'I-ElectricExpenseAmount', 'O', 'Tag']

# Loading training data 
with open ('df_ammend_ner_train', 'rb') as fp:      # enter dataset name
    TRAIN_DATA = pickle.load(fp)

# function for training the spacy model for custom entities
def dataTrain(model=None, new_model_name='new_model', output_dir=None, n_iter=500):
    """Setting up the pipeline and entity recognizer, and training the new entity."""
    if model is not None:
        nlp = spacy.load(model)  # load existing spacy model
        print("Loaded model '%s'" % model)
    else:
        nlp = spacy.blank('en')  # create blank Language class
        print("Created blank 'en' model")
    if 'ner' not in nlp.pipe_names:
        ner = nlp.create_pipe('ner')
        nlp.add_pipe(ner)
    else:
        ner = nlp.get_pipe('ner')

    for i in LABEL:
        ner.add_label(i)   # Add new entity labels to entity recognizer

    if model is None:
        optimizer = nlp.begin_training()
    else:
        optimizer = nlp.entity.create_optimizer()

    # Get names of other pipes to disable them during training to train only NER
    other_pipes = [pipe for pipe in nlp.pipe_names if pipe != 'ner']
    with nlp.disable_pipes(*other_pipes):  # only train NER
        for itn in range(n_iter):
            random.shuffle(TRAIN_DATA)
            losses = {}
            batches = minibatch(TRAIN_DATA, size=compounding(4., 32., 1.001))
            for batch in batches:
                texts, annotations = zip(*batch)
                nlp.update(texts, annotations, sgd=optimizer, drop=0.35,
                           losses=losses)
            print('Losses', losses)

    # Save the trained model
    cwd=str(os.getcwd())
    output_dir=os.path.join(cwd, 'model_data')  # give the output's folder path
    nlp.meta['name'] = 'spacy_trained_model'  # give your own model name
    nlp.to_disk(output_dir)
    print("Saved model to", output_dir)

dataTrain()


     
