import pandas as pd
import os
from simpletransformers.classification import ClassificationModel
from nltk import sent_tokenize
import spacy
from spacy.util import minibatch, compounding
import spacy
from spacy.gold import GoldParse
from spacy.scorer import Scorer

cwd = str(os.getcwd())
fp = open(os.path.join(cwd, 'test_lease','NAVS, Inc Original Lease.txt'))   # enter the test lease path
about_text = fp.read()

# split the lease into sentences
sentences = sent_tokenize(about_text)

# loading the data model for testing
output_dir=os.path.join(cwd, 'model_data')      # give the path of the trained model
model_data = spacy.load(output_dir)

labels = {}

# making predictions on each sentence
for i in range(0,len(sentences)):
    doc2 = model_data(sentences[i])     # predicting the data
    for ent in doc2.ents:
        labels[ent.label_[2:]] += ent.text + ' '



for label, text in labels.items():
    print('The %s is: %s' % (label, text))

print('')
print('The term clause is:', term_para)
print('')
#print('The common language for the term clause is:', com_lang_term)
#print('')
print('The alteration clause is:', alteration_para)
#print('')
#print('The common language for the alteration clause is:', com_lang_alteration)
