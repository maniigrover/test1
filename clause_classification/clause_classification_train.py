import pandas as pd
from simpletransformers.classification import ClassificationModel
from sklearn.metrics import f1_score, accuracy_score

# reading the training dataset
train_df = pd.read_csv('clause_dataset/clause_train.csv')

# replacing new line with space
train_df = train_df.replace({r'\s+$': '', r'^\s+': ''}, regex=True).replace(r'\n',  ' ', regex=True)

# setting the labels to integer value
train_df.loc[train_df['label'] == 'Other', 'label'] = 0
train_df.loc[train_df['label'] == 'USE', 'label'] = 1
train_df.loc[train_df['label'] == 'ALTERTN', 'label'] = 2
train_df.loc[train_df['label'] == 'ASGNSUB', 'label'] = 3
train_df.loc[train_df['label'] == 'TTMAINT', 'label'] = 4
train_df.loc[train_df['label'] == 'INSURNC', 'label'] = 5
train_df.loc[train_df['label'] == 'SUBORD', 'label'] = 6
train_df.loc[train_df['label'] == 'ESTOPEL', 'label'] = 7
train_df.loc[train_df['label'] == 'HOLDOVR', 'label'] = 8
train_df.loc[train_df['label'] == 'RELO', 'label'] = 9

train_df=train_df.dropna()
# print(train_df)

# reading the eval dataset
eval_df = pd.read_csv('clause_dataset/clause_eval.csv')

# replacing new line with space
eval_df = eval_df.replace({r'\s+$': '', r'^\s+': ''}, regex=True).replace(r'\n',  ' ', regex=True)

# setting the labels to integer value
eval_df.loc[eval_df['label'] == 'Other', 'label'] = 0
eval_df.loc[eval_df['label'] == 'USE', 'label'] = 1
eval_df.loc[eval_df['label'] == 'ALTERTN', 'label'] = 2
eval_df.loc[eval_df['label'] == 'ASGNSUB', 'label'] = 3
eval_df.loc[eval_df['label'] == 'TTMAINT', 'label'] = 4
eval_df.loc[eval_df['label'] == 'INSURNC', 'label'] = 5
eval_df.loc[eval_df['label'] == 'SUBORD', 'label'] = 6
eval_df.loc[eval_df['label'] == 'ESTOPEL', 'label'] = 7
eval_df.loc[eval_df['label'] == 'HOLDOVR', 'label'] = 8
eval_df.loc[eval_df['label'] == 'RELO', 'label'] = 9

eval_df=eval_df.dropna()
# print(eval_df)

# Create a ClassificationModel
model = ClassificationModel('bert', 'bert-base-cased', num_labels=10, args={'num_train_epochs': 40, 'reprocess_input_data': True, 'overwrite_output_dir': True, 'evaluate_during_training': True, 'output_dir':'output_clauses/'}, use_cuda=False)

# arguments for the model
# self.args = {
#     "output_dir": "outputs/",
#     "cache_dir": "cache_dir/",

#     "fp16": True,
#     "fp16_opt_level": "O1",
#     "max_seq_length": 128,
#     "train_batch_size": 8,
#     "gradient_accumulation_steps": 1,
#     "eval_batch_size": 8,
#     "num_train_epochs": 5,
#     "weight_decay": 0,
#     "learning_rate": 4e-5,
#     "adam_epsilon": 1e-8,
#     "warmup_ratio": 0.06,
#     "warmup_steps": 0,
#     "max_grad_norm": 1.0,

#     "logging_steps": 50,
#     "save_steps": 2000,

#     "overwrite_output_dir": False,
#     "reprocess_input_data": False,
#     "evaluate_during_training": False,

#     "process_count": cpu_count() - 2 if cpu_count() > 2 else 1,
#     "n_gpu": 1,
# }

# training the classification model
model.train_model(train_df, eval_df=eval_df)



